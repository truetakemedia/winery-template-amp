---
name: Wine Blending
date: 2019-02-16 18:00:00
description_markdown: >-
  Your chance to be a winemaker for a day! Enjoy a special tasting of multiple red wine varietals. Then, with help from our professional staff, craft your own unique wine blend using authentic vintner equipment. 
  
  Finish your experience by blending, bottling, corking and even labeling your custom wine. Create a single bottle, multiple bottles, or several cases to take home or share at a special event. Give us a call to Reserve your spot. Event Fee (including your custom bottle of wine) $75. 
event_image: /images/events/wine-blending.jpg
---
