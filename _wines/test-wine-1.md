---
title: Test-Wine-1
winename: Cabernet Sauvignon
description: This is a red cab with 80% this and 20% that
region: Napa
wine-color: Red
image_path: /images/wine/cab-reserve.jpg
video_path: 'https://www.youtube.com/embed/7lf5QDTdNUE'
featured: True
price: $40
---