---
title: Test-Wine-3
winename: Cabernet Franc
description: This is a red cab with 80% this and 20% that
region: South-Coast
wine-color: Red
image_path: /images/wine/cab-reserve.jpg
video_path: 
featured: true
price: $35
---