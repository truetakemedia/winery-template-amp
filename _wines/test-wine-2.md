---
title: Test-Wine-2
winename: Chardonnay
description: This is a white chardonnay with 80% this and 20% that
region: Lodi
wine-color: White
image_path: /images/wine/cab-reserve.jpg
video_path: 'https://www.youtube.com/embed/7lf5QDTdNUE'
featured: True
price: $40
---